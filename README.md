# docker-compose

Gitlab CI runner with docker-compose ready as specified in [Gitlab Documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor)

## Tags

- latest
  - Based on [docker](https://hub.docker.com/_/docker):latest
  - Rebuilt every week

## Usage

    ```yaml
    build:
        image: registry.gitlab.com/savadenn-public/runners/docker-compose:latest
        script:
          - docker-compose --version
    ```
